Debian-repository for Docker
============================

This project is based on https://github.com/glenux/docker-debian-repository [(Glenn Y. Rolland, aka Glenux)](http://www.glenux.net). Original docker image is out of date and doesn't work anymore.


This project can be used as a local repository for publishing deb files for use with apt - especially usefull as a gitlab ci service.

This docker box provides an apt repository based on the tool reprepro.
The repository is served by an nginx server.


Usage
-----

### Running the box

Run with 22 and 80 ports opened. No authentication is needed for ssh access - it's very unsecure, but very convenient for gitlab ci service.

	docker run -d -p 49160:22 -p 49161:80 piotrgawron/debian-repository


### Uploading packages

```
echo "PUT foobar_0.0.1_amd64.deb /docker/incoming" | sftp -o StrictHostKeyChecking=no -P 49160 user@localhost
```

You can trigger rebuilding repo indices by:
```
ssh -p 49160 root@localhost /usr/local/sbin/reprepro-import
```

Or you can wait a minute for cron job to do it automatically.

### Accessing the repository

Add the following line to your source list

	deb http://localhost:49161/ unstable main contrib non-free


License
-------

It is free software, and may be redistributed under the terms specified in the LICENSE file.

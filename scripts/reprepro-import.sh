#!/bin/sh

BASEDIR=/var/lib/reprepro
INCOMING=/docker/incoming
OUTDIR=/repository/debian

#
# Make sure we're in the apt/ directory
#
cd $INCOMING
cd ..

#set -x
reprepro --basedir $BASEDIR --outdir $OUTDIR createsymlinks unstable
#
#  See if we found any new packages
#
found=0
for i in $INCOMING/*.deb; do
  if [ -e $i ]; then
    found=`expr $found + 1`
  fi
done
#
#  If we found none then exit
#
if [ "$found" -lt 1 ]; then
   exit
fi


#
#  Now import each new package that we *did* find
#
for i in $INCOMING/*.deb; do
  reprepro -b $OUTDIR --confdir $BASEDIR/conf includedeb unstable $i
done
chown -R www-data:www-data $OUTDIR
